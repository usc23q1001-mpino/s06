# The goal of the capstone project is to create a simple employee ticketing systemusing classes and objecst.

# 1. Create a Person Class that is an abstract class that has the following methods:
#   - a. getFullName method
#   - b. addRequest method
#   - c. checkRequest method
#   - d. addUser method 

from abc import ABC, abstractclassmethod

class Person(ABC):
    @abstractclassmethod
    def getFullName(self):
        pass
    @abstractclassmethod
    def addRequest(self):
        pass
    @abstractclassmethod
    def checkRequest(self):
        pass
    @abstractclassmethod
    def addUser(self):
        pass
# 2. Create a Employee Class from Person with the following properties and methods:
#   - A. PROPERTIES
#       - firstName
#       - lastName
#       - email
#       - department
class Employee(Person):

    def __init__(self, firstName, lastName, email, department):
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department

#   - B. METHODS
#       - Setters
    def set_firstName(self, firstName):
        self._firstName = firstName

    def set_lastName(self, lastName):
        self._lastName = lastName

    def set_email(self, email):
        self._email = email

    def set_department(self, department):
        self._department = department

#       - Getters       
    def get_firstName(self):
        return f"{self._firstName}"

    def get_lastName(self):
        return f"{self._lastName}"

    def get_email(self):
        return f"{self._email}"

    def get_department(self):
        return f"{self._department}"

#   - C. Abstract methods (All methods just return Strings of simple text)
#       - checkRequest() - placeholder method
#       - addUser() - placeholder method
#       - login() - outputs "<Email> has logged in"
#       - logout() - outputs "<Email> has logged out"

    def checkRequest(self):
        pass

    def addUser(self):
        pass

    def addRequest(self):
        return "Request has been added"

    def login(self):
        return f"{self._email} has logged in"

    def logout(self):
        return f"{self._email} has logged out"

    def getFullName(self):
        return f"{self._firstName} {self._lastName}"

# 3. Create a TeamLead Class from Person with the following properties and methods:
#   - A. PROPERTIES
#       - firstName
#       - lastName
#       - email
#       - department
class TeamLead(Person):
    
    def __init__(self, firstName, lastName, email, department):
        super().__init__()
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department
        self._members = set()

#   - B. METHODS
#       - Setters
    def set_firstName(self, firstName):
        self._firstName = firstName

    def set_lastName(self, lastName):
        self._lastName = lastName

    def set_email(self, email):
        self._email = email

    def set_department(self, department):
        self._department = department

#       - Getters       
    def get_firstName(self):
        print(f"TeamLead first name is {self._firstName}.")

    def get_lastName(self):
        print(f"TeamLead last name is {self._lastName}.")

    def get_email(self):
        print(f"TeamLead email is {self._email}.")

    def get_department(self):
        print(f"TeamLead belongs to department of {self._department}.")

#   - C. Abstract methods (All methods just return Strings of simple text)
#       - checkRequest() - placeholder method
#       - addUser() - placeholder method
#       - login() - outputs "<Email> has logged in"
#       - logout() - outputs "<Email> has logged out"
#       - addMember() - adds an employeeto the members list

    def checkRequest(self):
        pass

    def addUser(self):
        pass

    def login(self):
        printf(f"{self._email} has logged IN.")

    def logout(self):
        printf(f"{self._email} has logged OUT.")

    def addMember(self, employee):
        self._members.add(employee)

    def getMembers(self):
        return self._members

    def addRequest(self):
        return "Request has been added"

    def getFullName(self):
        return f"{self._firstName} {self._lastName}"

# 4. Create a Admin Class from Person with the following properties and methods:
#   - A. PROPERTIES
#       - firstName
#       - lastName
#       - email
#       - department
class Admin(Person):
        
    def __init__(self, firstName, lastName, email, department):
        super().__init__()
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department

#   - B. METHODS
#       - Setters
    def set_firstName(self, firstName):
        self._firstName = firstName

    def set_lastName(self, lastName):
        self._lastName = lastName

    def set_email(self, email):
        self._email = email

    def set_department(self, department):
        self._department = department

#       - Getters       
    def get_firstName(self):
        print(f"Admin first name is {self._firstName}.")

    def get_lastName(self):
        print(f"Admin last name is {self._lastName}.")

    def get_email(self):
        print(f"Admin email is {self._email}.")

    def get_department(self):
        print(f"Admin belongs to department of {self._department}.")

#   - C. Abstract methods (All methods just return Strings of simple text)
#       - checkRequest() - placeholder method
#       - addUser() - placeholder method
#       - login() - outputs "<Email> has logged in"
#       - logout() - outputs "<Email> has logged out"
#       - addMember() - adds an employeeto the members list

    def checkRequest(self):
        pass

    def addUser(self):
        return "User has been added"

    def login(self):
        printf(f"{self._email} has logged in.")

    def logout(self):
        printf(f"{self._email} has logged in.")

    def addRequest(self):
        pass

    def getFullName(self):
        return f"{self._firstName} {self._lastName}"


# 5. Create a Request Class that has the following properties and methods:
#   - A. PROPERTIES
#       - name
#       - requester
#       - dateRequested
#       - status

class Request():

    def __init__(self, name, requester, dateRequested):
        super().__init__()
        self._name = name
        self._requester = requester
        self._dateRequested = dateRequested
        self._status = ''
        
#   - B. METHODS
#       - updateRequest
#       - closeRequest
#       - cancelRequest

#       - Setters
    def set_name(self, name):
        self._name = name

    def set_requester(self, requester):
        self._requester = requester

    def set_dateRequested(self, dateRequested):
        self._dateRequested = dateRequested

    def set_status(self, status):
        self._status = status

#       - Getters       
    def get_name(self):
        print(f"Request name:{self._name}")

    def get_requester(self):
        print(f"Requester name: {self._requester}")

    def get_dateRequested(self):
        print(f"The date of the request: {self._dateRequested}")

    def get_status(self):
        print(f"Status of the request: {self._status}")

    def updateRequest(self):
        pass

    def closeRequest(self):
        pass
        return f"{self._name} request has been closed"

    def cancelRequest(self):
        pass


# TEST CASES
employee1 = Employee("Jhon", "Doe", "djhon@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1    = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamLead1 = TeamLead("Micheal", "Specter", "smicheal@mail.com", "Sales")
req1      = Request("New hire orientation", teamLead1, "27-Jul-2021")
req2      = Request("Laptop repair", employee1, "1-Jul-2021")

assert employee1.getFullName() == "Jhon Doe", "Full name should be Jhon Doe"
assert admin1.getFullName()    == "Monika Justin", "Full name should be Monika Justin"
assert teamLead1.getFullName() == "Micheal Specter", "Full name should be Micheal Specter"

assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.addRequest() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"

teamLead1.addMember(employee3)
teamLead1.addMember(employee4)

for indiv_emp in teamLead1.getMembers():
    print(indiv_emp.getFullName())

assert admin1.addUser() == "User has been added"

req2.set_status("closed")
print(req2.closeRequest())
